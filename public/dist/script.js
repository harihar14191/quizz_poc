/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 10);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */,
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Created by Shashank on 1/31/2017.
 */

// Angular module, defining routes for the app

window.recruitementApp = angular.module('quizzApp', ['ngRoute', 'ngCookies']);
recruitementApp.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/login', {
        templateUrl: 'login.html',
        controller: 'logInController'
    }).when('/addQuestions', {
        templateUrl: 'addQuestions.html',
        controller: 'logInController'
    }).when('/addAnswers', {
        templateUrl: 'addAnswers.html',
        controller: 'logInController'
    }).otherwise({ redirectTo: '/recruitement' });
}]);
recruitementApp.run(['$rootScope', '$route', '$location', '$window', function ($rootScope, $route, $location, $window) {}]);

/***/ }),
/* 2 */,
/* 3 */,
/* 4 */,
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Created by Shashank on 2/28/2017.
 */

/*   addQuestions Controller */

recruitementApp.controller('addQuestionsController', ['$scope', '$http', '$location', function ($scope, $http, $location) {
    $scope.QuestionList = [];

    $scope.addQuestons = function (index) {
        $scope.QuestionList.push({ 2: 2 });
        $scope.initAnswer(index + 1);
    };

    $scope.removeQuestons = function (index) {
        $scope.QuestionList.splice(index, 1);
    };

    $scope.initQuestion = function () {
        $scope.QuestionList.push({ 1: 1 });
        $scope.initAnswer(0);
    };

    $scope.addAnswers = function (index) {
        debugger;
        $scope.QuestionList[index].AnswerList.push({ 2: 2 });
    };

    $scope.removeAnswers = function (index, childIndex) {
        $scope.QuestionList[index].AnswerList.splice(childIndex, 1);
    };

    $scope.initAnswer = function (index) {
        $scope.QuestionList[index].AnswerList = [];
        $scope.QuestionList[index].AnswerList.push({ 1: 1 });
    };
    $scope.initQuestion();
}]);

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Created by Shashank on 2/28/2017.
 */

/*   logIn Controller */
recruitementApp.controller('logInController', ['$scope', '$http', '$cookies', '$location', function ($scope, $http, $cookies, $location) {
  $scope.logInUser = function () {
    debugger;
    $location.path('addQuestions');
  };
}]);

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Created by Shashank on 2/28/2017.
 */

recruitementApp.directive('loginControl', function () {
  debugger;
  return {
    restrict: 'EA',
    scope: {
      login: '=',
      loginfun: "&"
    },
    templateUrl: '../partials/userLogIn.html',
    link: function link(scope, elm, attr) {}
  };
});

/***/ }),
/* 8 */,
/* 9 */,
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(1);
__webpack_require__(6);
__webpack_require__(5);
module.exports = __webpack_require__(7);


/***/ })
/******/ ]);