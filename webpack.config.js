var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
const OpenBrowserPlugin = require('open-browser-webpack-plugin');
var config = {
    entry: {
        'script':['./public/app.js','./public/js/controller/logInController.js','./public/js/controller/addQuestionsController.js',
            './public/js/directive/userLogIn.js'],
        'vendor':['jquery','angular','ngRoute','ngCookie', 'jqueryUI']

    },

    output: {
        path: __dirname+'/public/dist',
        filename: '[name].js'
    },


    module: {
        loaders: [

            {
                test: /\.js?$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015','stage-1']
                }
            },
            // Extract css files
            {
                test:/(\.css|\.scss)$/,
                exclude: /node_modules/,
                loader:ExtractTextPlugin.extract({
                    fallback:'style-loader',
                    loader:'style-loader!css-loader!sass-loader'
                })
            },

           /* {
                test: /\.less$/,
                loader: ExtractTextPlugin.extract("style-loader", "css-loader!less-loader")
            },*/
           /* {test: /(\.css|\.scss)$/, loaders: ['style-loader', 'css-loader?sourceMap', 'sass-loader?sourceMap']},*/

            {test: /\.(jpe?g|png|gif)$/i, loader: 'file-loader?name=[name].[ext]'}
            /*'postcss-loader'*/
        ]
        /*'postcss-loader'*/
    },
    resolve: {
        alias: {
            jquery: path.join(__dirname, "public/js/jquery.js"),
            angular:path.join(__dirname, "public/js/angular.min.js"),
            ngRoute: path.join(__dirname, "public/js/angular-route.min.js"),
            ngCookie: path.join(__dirname, "public/js/angular-cookies.min.js"),
            jqueryScroll: path.join(__dirname, "public/js/jquery.scrollTo-1.4.2-min.js"),
            jqueryLocalScroll: path.join(__dirname, "public/js/jquery.localscroll-1.2.7-min.js"),
            jqueryUI: path.join(__dirname, "public/js/libs/jquery-ui-1.10.3.min.js")
        },
    },

    plugins: [
        new ExtractTextPlugin("styleBundle.css")
    ]
}

module.exports = config;