/**
 * Created by Shashank on 3/12/2017.
 */
'use restrict'

/**
 * Created by Shashank on 2/17/2017.
 */
let mongoose = require('mongoose');

let menuSchema = new mongoose.Schema({
  userType: {type: String, required: true},
  menuName: {type: String, required: true}
})

module.exports = mongoose.model('Menus',menuSchema);
