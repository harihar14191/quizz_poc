/**
 * Created by Shashank on 2/17/2017.
 */
let mongoose = require('mongoose');

let userSchema = new mongoose.Schema({
    emailID: {type: String, required: true},
    panNumber: {type: String, required: true},
    hash:String,
    salt:String,
    companyName: String,
    employerType: String,
    companyWebsite: String,
    companyLinkedin: String,
    companyFacebook: String,
    companyAddress: String,
    companyLocation: String,
    otherLocation: String,
    resumedDbAccess:String,
    cardType:String,
    nameOnCard:String,
    cardNo:String,
    cvvNO:String,
    expDateMonth:Number,
    expDateYear:Number,
    contactPerson: String,
    designation:String,
    contactEmailID:String,
    contactNo:String
})

module.exports = mongoose.model('Users',userSchema);